base_tests() {
    # -------------------------------------------------------------------------
    TEST "Base case"

    $REAL_COMPILER -c -o reference_test1.o test1.c

    $CCACHE_COMPILE -c test1.c -o test1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1
    expect_equal_object_files reference_test1.o test1.o

    $CCACHE_COMPILE -c test1.c -o test1.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1
    expect_equal_object_files reference_test1.o test1.o

    # -------------------------------------------------------------------------
    TEST "Debug option"

    $CCACHE_COMPILE -c test1.c -o test1.o -g
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c -o test1.o -g
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    $REAL_COMPILER -c -o reference_test1.o test1.c -g
    expect_equal_object_files reference_test1.o test1.o

    # -------------------------------------------------------------------------
    TEST "Output option"

    $CCACHE_COMPILE -c test1.c -o test.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c -o foo.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    $REAL_COMPILER -c -o reference_test1.o test1.c
    expect_equal_object_files reference_test1.o foo.o

    # -------------------------------------------------------------------------
    TEST "Output option without space"

    $CCACHE_COMPILE -c test1.c -o test1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c -otest1.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c -odir
    expect_stat 'hit' 2
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c -optf
    expect_stat 'hit' 3
    expect_stat 'miss' 1

    $REAL_COMPILER -c -o reference_test1.o test1.c
    expect_equal_object_files reference_test1.o dir
    expect_equal_object_files reference_test1.o ptf

    # -------------------------------------------------------------------------
    TEST "No input file"

    $CCACHE_COMPILE -c foo.c -o foo.o 2>/dev/null
    $CCACHE_COMPILE -c foo.c -o foo.o 2>/dev/null

    # ------------------------------------------------------------------------

    TEST "Called for preprocessing"

    $CCACHE_COMPILE -E -c test1.c -o test >/dev/null 2>&1
    $CCACHE_COMPILE -E -c test1.c -o test >/dev/null 2>&1
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "Multiple source files"

    touch test2.c
    $CCACHE_COMPILE -c test1.c test2.c
    expect_stat 'hit' 0
    expect_stat 'miss' 1
    

    # ------------------------------------------------------------------------
    TEST "Multiple source files"

    touch test2.c
    $CCACHE_COMPILE -c test1.c test2.c
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.c test2.c
    expect_stat 'hit' 2
    expect_stat 'miss' 2


    # ------------------------------------------------------------------------

    TEST "Bad compiler arguments"

    $CCACHE_COMPILE -c test1.c -o test1.o -I 2>/dev/null
    $CCACHE_COMPILE -c test1.c -o test1.0 -I 2>/dev/null
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "Unsupported source language"

    ln -f test1.c test1.ccc
    $CCACHE_COMPILE -c test1.ccc -o test1.o 2>/dev/null
    $CCACHE_COMPILE -c test1.ccc -o test1.o 2>/dev/null
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "Unsupported compiler option"

    $CCACHE_COMPILE -M foo -c test1.c -o test1.o >/dev/null 2>&1
    $CCACHE_COMPILE -M foo -c test1.c -o test1.o >/dev/null 2>&1
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "Output to a non-regular file"

    mkdir testd
    $CCACHE_COMPILE -o testd -c test1.c >/dev/null 2>&1
    $CCACHE_COMPILE -o testd -c test1.c >/dev/null 2>&1
    rmdir testd >/dev/null 2>&1
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "Direct .i compile"

    $CCACHE_COMPILE -c test1.c -o test.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1


    $REAL_COMPILER -c test1.c -E >test1.i
    $CCACHE_COMPILE -c test1.i -o test1.o

    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "-x c"

    ln -f test1.c test1.ccc

    $CCACHE_COMPILE -x c -c test1.ccc -o test1.o
    $CCACHE_COMPILE -x c -c test1.ccc -o test1.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    # -------------------------------------------------------------------------
    TEST "-x none"

    $CCACHE_COMPILE -x assembler -x none -c test1.c -o test1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -x assembler -x none -c test1.c -o test1.o

    expect_stat 'hit' 1
    expect_stat 'miss' 1

    # -------------------------------------------------------------------------
    TEST "-S"

    $CCACHE_COMPILE -S test1.c -o test1.s
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -S test1.c -o test1.s
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c test1.s -o test1.o
    expect_stat 'hit' 1
    expect_stat 'miss' 2

    $CCACHE_COMPILE -c test1.s -o test1.o
    expect_stat 'hit' 2
    expect_stat 'miss' 2


    # -------------------------------------------------------------------------
    TEST "-P"

    # Check that -P disables ccache. (-P removes preprocessor information in
    # such a way that the object file from compiling the preprocessed file will
    # not be equal to the object file produced when compiling without ccache.)

    $CCACHE_COMPILE -c -P test1.c -o test1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1


    $CCACHE_COMPILE -c -P test1.c -o test1.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    $REAL_COMPILER -c -P test1.c -o reference_test1.o 
    expect_equal_object_files reference_test1.o test1.o

    # -------------------------------------------------------------------------
    TEST "-ftest-coverage"

    cat <<EOF >code.c
int test() { return 0; }
EOF

    $CCACHE_COMPILE -c -fprofile-arcs -ftest-coverage code.c -o code.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1
    expect_file_exists code.gcno

    rm code.gcno

    $CCACHE_COMPILE -c -fprofile-arcs -ftest-coverage code.c -o code.o
    expect_stat 'hit' 0
    expect_stat 'miss' 2
    expect_file_exists code.gcno

    # -------------------------------------------------------------------------
    TEST "-fstack-usage"

    cat <<EOF >code.c
int test() { return 0; }
EOF

    if $COMPILER_TYPE_GCC; then
        $CCACHE_COMPILE -c -fstack-usage code.c -o code.o
        expect_stat 'hit' 0
        expect_stat 'miss' 1
        expect_file_exists code.su

        rm code.su

        $CCACHE_COMPILE -c -fstack-usage code.c -o code.o
        expect_stat 'hit' 0
        expect_stat 'miss' 2
        expect_file_exists code.su
    fi
    # -------------------------------------------------------------------------
    TEST "Empty source file"

    touch empty.c

    $CCACHE_COMPILE -c empty.c -o empty.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c empty.c -o empty.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    # -------------------------------------------------------------------------
    TEST "Empty include file"

    touch empty.h
    cat <<EOF >include_empty.c 
#include "empty.h"
EOF
    backdate empty.h
    $CCACHE_COMPILE -c include_empty.c -o include_empty.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c include_empty.c -o include_empty.o
    expect_stat 'hit' 1
    expect_stat 'miss' 1

    # -------------------------------------------------------------------------
    TEST "__TIME__ in source file disables direct mode"

    cat <<EOF >time.c
#define time __TIME__
int test;
EOF

    $CCACHE_COMPILE -c time.c -o time.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1


    $CCACHE_COMPILE -c time.c -o time.o
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    TEST "__TIME__ in include file disables direct mode"

    cat <<EOF >time.h
#define time __TIME__
int test;
EOF
    backdate time.h

    cat <<EOF >time_h.c
#include "time.h"
EOF

    $CCACHE_COMPILE -c time_h.c -o time_h.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c time_h.c -o time_h.o
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    # -------------------------------------------------------------------------
    
    TEST "Test -B handling"

    expect_stat 'hit' 1
    echo "We do not do -B handling"
    
    # -------------------------------------------------------------------------
    TEST "-finput-charset"

    printf '#include <wchar.h>\nwchar_t foo[] = L"\xbf";\n' >latin1.c

    $CCACHE_COMPILE -c -finput-charset=latin1 latin1.c -o latin1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 1

    $CCACHE_COMPILE -c -finput-charset=latin1 latin1.c -o latin1.o
    expect_stat 'hit' 0
    expect_stat 'miss' 2

    echo "Do we do -finput-charset handling??"
    
}

# =============================================================================

SUITE_base_SETUP() {
    generate_code 1 test1.c
}

SUITE_base() {
    base_tests
}
